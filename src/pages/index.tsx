import * as React from "react";
import Link from "gatsby-link";

// Please note that you can use https://github.com/dotansimha/graphql-code-generator
// to generate all types from graphQL schema
interface INode {
  node: {
    id: string;
    name: string;
  };
}
interface IndexPageProps {
  data: {
    allClientsJson: {
      edges: INode[];
    };
  };
}

export default class extends React.Component<IndexPageProps, {}> {
  constructor(props: IndexPageProps, context: any) {
    super(props, context);
  }
  public render() {
    const clients = this.props.data.allClientsJson.edges;

    const listClients = clients.map(client => (
      <li key={client.node.id}>{client.node.name}</li>
    ));

    return (
      <div>
        <h1>Hi people</h1>
        <p>
          Welcome to your new <strong>Gatsby</strong> site.
        </p>
        <ul>{listClients}</ul>
        <p>Now go build something great.</p>
        <Link to="/page-2/">Go to page 2</Link>
      </div>
    );
  }
}

export const pageQuery = graphql`
  query IndexQuery {
    allClientsJson {
      edges {
        node {
          id
          name
        }
      }
    }
  }
`;
